
package Modelo;

import Config.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PersonalDAO {
    Conexion cn=new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    
    public Personal validar(String user, String dni){
        Personal em=new Personal();
        String sql="select * from empleado where User=? and Dni=?";
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, dni);
            rs=ps.executeQuery();
            
            while (rs.next()){
            em.setId(rs.getInt("IdEmpleado"));
            em.setUser(rs.getString("User"));
            em.setDni(rs.getString("Dni"));
            em.setNombre(rs.getString("Nombres")); 
            }
        } catch (Exception e) {
        }
                
           return em;
    }
}
